import React from 'react'
import { useLocation, useSearchParams } from 'react-router-dom'

export const CourseList = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const location = useLocation();
  console.log(location);
  console.log(searchParams.has('q'));
  console.log(searchParams.get('q'));
  const searchFor = searchParams.get('q');
  return (
    <div className='component'>
      CourseList | {searchFor}
    </div>
  )
}

// Query: /courses?q=java