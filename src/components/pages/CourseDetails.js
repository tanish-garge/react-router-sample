import React from 'react'
import { useParams } from 'react-router-dom'

export const CourseDetails = () => {
  const params = useParams();
  console.log(params);
  return (
    <div className='component'>CourseDetails | { `${params.category} ${params.id}` }</div>
  )
}

// Query: courses/science/1