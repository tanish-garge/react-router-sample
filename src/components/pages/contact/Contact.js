import React from 'react'
import { Outlet, useNavigate } from 'react-router-dom'

export const Contact = () => {
  const navigate = useNavigate(); 
  function handleSumit() {
    // Perform Post request with data
    navigate("/") // jab sumbit karne ke baad redirect karna padega kisi jagaha pe redirect akrna hoga usliye useNavigate() hook
  }
  return (
    <div className='component'>
      Contact Us 
      <Outlet />
      <div>
        <button onClick={handleSumit}>Send Message</button>
      </div>
    </div>
  )
}

