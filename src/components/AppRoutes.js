import React from 'react'
import { Navigate, Route, Routes } from 'react-router-dom';
import { Admin, Contact, ContactEu, ContactIn, CourseDetails, CourseList, Home, Login, Others, PageNotFound } from './pages';

export const AppRoutes = () => {
    const isAdmin = false; // Toggle this to test Navigate Component
    return (
        <Routes>
        <Route path="/" element={ <Home />} />
        <Route path="/courses" element={ <CourseList />} />
        <Route path='courses/:category/:id' element={<CourseDetails/>}/>
        <Route path="/contact-us" element={ <Contact />}>
            <Route path='in' element={<ContactIn />} />
            <Route path='eu' element={<ContactEu />} />
            <Route path='*' element={<Others />} />  
        </Route> 
        <Route path="/protected-route" element={ isAdmin ? <Admin /> : <Navigate to="/login" />} />
        <Route path="/login" element={ <Login />} />
        <Route path="*" element={ <PageNotFound />} />
    </Routes>
    )
}
