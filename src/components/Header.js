import React from "react";
import { Link, NavLink } from "react-router-dom";
import Logo from "../ReactLogo.svg"
import "./Header.css";

export const Header = () => {
    return (
    <div className="header component">
        <Link to="/" className="brand">
            <img className="logo" src={Logo} alt="React Logo" />
            <h2>React Router</h2>
        </Link>
        <nav>
            <NavLink to="/" className={(({isActive}) => isActive ? 'current navlink' : 'navlink')}>Home</NavLink>
            <NavLink to="/courses" className={(({isActive}) => isActive ? 'current navlink' : 'navlink')}>Courses</NavLink>
            <NavLink to="/contact-us" className={(({isActive}) => isActive ? 'current navlink' : 'navlink')}>Contact</NavLink>
        </nav>
    </div>
    )
};
