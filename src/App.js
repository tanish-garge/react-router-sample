import React from "react";
import { AppRoutes, Footer, Header } from "./components";

export default function App() {
    return (
        <div class="App">
            <Header />
              <AppRoutes />
            <Footer />
        </div>
    );
}
